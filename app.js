const express = require("express");
const app = express();
app.get("/", (req, res) => {
  res.send("Hello, world!");
});
app.get("/homepage", (req, res) => {
  res.send("Hello, this is homepage!");
});
app.get("/todo", (req, res) => {
  res.send("Hello, this is todo page!");
});
app.listen(3000, () => {
  console.log("Server is listening on port 3000");
});
